import React, { useState } from "react";
import { Form, Row, Col, Button } from "react-bootstrap";
import "./css/EditUser.css"; // Remplacez "EditUser.css" par le nom de votre fichier CSS

const EditUser = ({ user, showDeleteConfirmation }) => {
  const [formData, setFormData] = useState({
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    gender: user.gender,
    username: user.username,
    role: user.role,
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // Perform actions with the updated data (e.g., send it to the server)
    console.log(formData);
  };

  return (
    <div className="edit-user-container">
      <h2 className="edit-user-title">Modifier l'utilisateur</h2>
      <Form className="edit-user-form" onSubmit={handleSubmit}>
        <Form.Group
          as={Row}
          controlId="formFirstName"
          className="edit-user-group">
          <Form.Label column sm="2" className="edit-user-label">
            Prénom
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="firstName"
              placeholder="Prénom"
              onChange={handleChange}
              value={formData.firstName}
              className="edit-user-control"
            />
          </Col>
        </Form.Group>
        <Form.Group
          as={Row}
          controlId="formLastName"
          className="edit-user-group">
          <Form.Label column sm="2" className="edit-user-label">
            Nom
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="lastName"
              placeholder="Nom"
              onChange={handleChange}
              value={formData.lastName}
              className="edit-user-control"
            />
          </Col>
        </Form.Group>
        <Form.Group
          as={Row}
          controlId="formUsername"
          className="edit-user-group">
          <Form.Label column sm="2" className="edit-user-label">
            Nom d'utilisateur
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="username"
              placeholder="Nom d'utilisateur"
              onChange={handleChange}
              value={formData.username}
              className="edit-user-control"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formEmail" className="edit-user-group">
          <Form.Label column sm="2" className="edit-user-label">
            Email
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="email"
              name="email"
              placeholder="Email"
              onChange={handleChange}
              value={formData.email}
              className="edit-user-control"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formGender" className="edit-user-group">
          <Form.Label column sm="2" className="edit-user-label">
            Genre
          </Form.Label>
          <Col sm="10">
            <Form.Control
              as="select"
              name="gender"
              onChange={handleChange}
              value={formData.gender}
              className="edit-user-control">
              <option value="">Sélectionnez le sexe</option>
              <option value="male">Homme</option>
              <option value="female">Femme</option>
              <option value="other">Autre</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formRole" className="edit-user-group">
          <Form.Label column sm="2" className="edit-user-label">
            Rôle
          </Form.Label>
          <Col sm="10">
            <Form.Control
              as="select"
              name="role"
              onChange={handleChange}
              value={formData.role}
              className="edit-user-control">
              <option value="">Sélectionnez le rôle</option>
              <option value="admin">Admin</option>
              <option value="user">Utilisateur</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group
          as={Row}
          controlId="formButtons"
          className="edit-user-group edit-user-buttons">
          <Col sm="2"></Col>
          <Col sm="10" className="d-flex justify-content-between">
            <Button
              variant="danger"
              style={{ marginTop: "9px", marginRight: "10px" }}
              className="edit-user-button"
              onClick={() => showDeleteConfirmation(user)}>
              Supprimer
            </Button>
            <Button
              variant="primary"
              type="submit"
              className="edit-user-button">
              Enregistrer
            </Button>
          </Col>
        </Form.Group>
      </Form>
    </div>
  );
};

export default EditUser;
