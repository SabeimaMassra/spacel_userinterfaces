import React, { useState } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import "./css/CreateUser.css"; // Remplacez "CreateUser.css" par le nom de votre fichier CSS

const CreateUser = () => {
  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    gender: "",
    password: "",
    confirmPassword: "",
    role: "",
    username: "",
  });

  const [errors, setErrors] = useState({});

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const validateForm = () => {
    let tempErrors = {};

    tempErrors.firstName = user.firstName ? "" : "Le prénom est requis.";
    tempErrors.lastName = user.lastName ? "" : "Le nom est requis.";
    tempErrors.email = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/.test(
      user.email
    )
      ? ""
      : "Email n'est pas valide.";
    tempErrors.password =
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z])(?=.*[!@#$%^&*]).{8,}$/.test(
        user.password
      )
        ? ""
        : "Le mot de passe doit contenir au moins 8 caractères, 1 chiffre, 1 majuscule et 1 caractère spécial.";
    tempErrors.confirmPassword =
      user.password === user.confirmPassword
        ? ""
        : "Les mots de passe ne correspondent pas.";

    tempErrors.gender = user.gender ? "" : "Veuillez sélectionner le sexe.";
    tempErrors.role = user.role ? "" : "Veuillez sélectionner le rôle.";
    tempErrors.username = user.username
      ? ""
      : "Le nom d'utilisateur est requis.";

    setErrors({ ...tempErrors });

    return Object.values(tempErrors).every((x) => x === "");
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValid = validateForm();
    if (isValid) {
      const userData = {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        gender: user.gender,
        password: user.password,
        role: user.role,
        username: user.username,
      };
      alert(JSON.stringify(userData));
      setUser({
        firstName: "",
        lastName: "",
        email: "",
        gender: "",
        password: "",
        confirmPassword: "",
        role: "",
        username: "",
      });
    }
  };

  return (
    <Container className="create-user-container">
      <h2>Créer un utilisateur</h2>
      <Form onSubmit={handleSubmit} className="create-user-form">
        <Form.Group
          as={Row}
          controlId="formFirstName"
          className="create-user-group">
          <Form.Label column sm="2" className="create-user-label">
            Prénom
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="firstName"
              placeholder="Prénom"
              onChange={handleChange}
              isInvalid={!!errors.firstName}
              value={user.firstName}
              className="create-user-control"
            />
            <Form.Control.Feedback
              type="invalid"
              className="create-user-feedback">
              {errors.firstName}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>
        <Form.Group
          as={Row}
          controlId="formLastName"
          className="create-user-group">
          <Form.Label column sm="2" className="create-user-label">
            Nom
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="lastName"
              placeholder="Nom"
              onChange={handleChange}
              isInvalid={!!errors.lastName}
              value={user.lastName}
              className="create-user-control"
            />
            <Form.Control.Feedback
              type="invalid"
              className="create-user-feedback">
              {errors.lastName}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>
        <Form.Group
          as={Row}
          controlId="formUsername"
          className="create-user-group">
          <Form.Label column sm="2" className="create-user-label">
            Nom d'utilisateur
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="username"
              placeholder="Nom d'utilisateur"
              onChange={handleChange}
              isInvalid={!!errors.username}
              value={user.username}
              className="create-user-control"
            />
            <Form.Control.Feedback
              type="invalid"
              className="create-user-feedback">
              {errors.username}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>
        <Form.Group
          as={Row}
          controlId="formEmail"
          className="create-user-group">
          <Form.Label column sm="2" className="create-user-label">
            Email
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="email"
              name="email"
              placeholder="Email"
              onChange={handleChange}
              isInvalid={!!errors.email}
              value={user.email}
              className="create-user-control"
            />
            <Form.Control.Feedback
              type="invalid"
              className="create-user-feedback">
              {errors.email}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>
        <Form.Group
          as={Row}
          controlId="formGender"
          className="create-user-group">
          <Form.Label column sm="2" className="create-user-label">
            Genre
          </Form.Label>
          <Col sm="10">
            <Form.Control
              as="select"
              name="gender"
              onChange={handleChange}
              isInvalid={!!errors.gender}
              value={user.gender}
              className="create-user-control">
              <option value="">Sélectionnez le sexe</option>
              <option value="male">Homme</option>
              <option value="female">Femme</option>
              <option value="other">Autre</option>
            </Form.Control>
            <Form.Control.Feedback
              type="invalid"
              className="create-user-feedback">
              {errors.gender}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>
        <Form.Group
          as={Row}
          controlId="formPassword"
          className="create-user-group">
          <Form.Label column sm="2" className="create-user-label">
            Mot de passe
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="password"
              name="password"
              placeholder="Mot de passe"
              onChange={handleChange}
              isInvalid={!!errors.password}
              value={user.password}
              className="create-user-control"
            />
            <Form.Control.Feedback
              type="invalid"
              className="create-user-feedback">
              {errors.password}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>
        <Form.Group
          as={Row}
          controlId="formConfirmPassword"
          className="create-user-group">
          <Form.Label column sm="2" className="create-user-label">
            Confirmez le mot de passe
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="password"
              name="confirmPassword"
              placeholder="Confirmez le mot de passe"
              onChange={handleChange}
              isInvalid={!!errors.confirmPassword}
              value={user.confirmPassword}
              className="create-user-control"
            />
            <Form.Control.Feedback
              type="invalid"
              className="create-user-feedback">
              {errors.confirmPassword}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formRole" className="create-user-group">
          <Form.Label column sm="2" className="create-user-label">
            Rôle
          </Form.Label>
          <Col sm="10">
            <Form.Control
              as="select"
              name="role"
              onChange={handleChange}
              isInvalid={!!errors.role}
              value={user.role}
              className="create-user-control">
              <option value="">Sélectionnez le rôle</option>
              <option value="admin">Admin</option>
              <option value="user">Utilisateur</option>
            </Form.Control>
            <Form.Control.Feedback
              type="invalid"
              className="create-user-feedback">
              {errors.role}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>
        <Button variant="primary" type="submit" className="create-user-button">
          Créer un utilisateur
        </Button>
      </Form>
    </Container>
  );
};

export default CreateUser;
