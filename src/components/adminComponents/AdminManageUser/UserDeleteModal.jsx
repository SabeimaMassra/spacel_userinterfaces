import React from "react";
import { Modal, Button } from "react-bootstrap";

const UserDeleteModal = ({ user, show, onHide, onDelete }) => {
  const handleDelete = () => {
    onDelete(user.id); // Appel de la fonction onDelete avec l'ID de l'utilisateur
    onHide(); // Fermer le modal après la suppression
  };

  if (user === null) {
    return null; // Retourner null si l'utilisateur n'est pas défini
  }

  return (
    <Modal show={show} onHide={onHide} centered>
      <Modal.Header closeButton>
        <Modal.Title>Supprimer l'utilisateur</Modal.Title>
        <button
          type="button"
          class="close"
          data-dismiss="modal"
          aria-label="Close"
          onClick={onHide}
          style={{ fontSize: "1.5rem" }}
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </Modal.Header>
      <Modal.Body>
        Êtes-vous sûr de vouloir supprimer l'utilisateur {user.firstName}{" "}
        {user.lastName} ?
      </Modal.Body>
      <Modal.Footer>
        <Button
          style={{ backgroundColor: "#FF6464" }}
          variant="secondary"
          onClick={onHide}
        >
          Annuler
        </Button>
        <Button
          style={{ backgroundColor: "#1EAAF1" }}
          variant="danger"
          onClick={handleDelete}
        >
          Supprimer
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default UserDeleteModal;
