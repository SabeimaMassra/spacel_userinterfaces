import React, { useState } from "react";
import { Form, Button, ListGroup, Alert } from "react-bootstrap";
import { AiOutlineCloseCircle } from "react-icons/ai";
import "./CreateFormation.css";
const CreateFormation = () => {
  const [formationName, setFormationName] = useState("");
  const [selectedLO, setSelectedLO] = useState("");
  const [los, setLos] = useState([]);
  const [duplicateError, setDuplicateError] = useState(false);

  const handleFormationNameChange = (event) => {
    setFormationName(event.target.value);
  };

  const handleLOSelect = (event) => {
    setSelectedLO(event.target.value);
  };

  const handleLOAdd = () => {
    if (selectedLO !== "" && !los.includes(selectedLO)) {
      setLos([...los, selectedLO]);
      setSelectedLO("");
      setDuplicateError(false);
    } else {
      setDuplicateError(true);
    }
  };

  const handleLORemove = (index) => {
    const updatedLos = [...los];
    updatedLos.splice(index, 1);
    setLos(updatedLos);
  };

  const handleFormationSubmit = (event) => {
    event.preventDefault();

    // Envoyer les données de la formation au serveur ou effectuer d'autres actions
    console.log("Nom de la formation :", formationName);
    console.log("LOs sélectionnés :", los);

    // Réinitialiser le formulaire
    setFormationName("");
    setLos([]);
  };

  return (
    <div className="container">
      <h1>Créer une formation</h1>
      <Form onSubmit={handleFormationSubmit}>
        <Form.Group controlId="formationName">
          <Form.Label>Nom de la formation :</Form.Label>
          <Form.Control
            type="text"
            value={formationName}
            onChange={handleFormationNameChange}
            required
          />
        </Form.Group>
        <Form.Group controlId="selectedLO">
          <Form.Label>Ajouter un LO :</Form.Label>
          <Form.Control
            as="select"
            value={selectedLO}
            onChange={handleLOSelect}
            required>
            <option value="">Sélectionner un LO</option>
            <option value="LO1">LO1</option>
            <option value="LO2">LO2</option>
            <option value="LO3">LO3</option>
            {/* Ajouter d'autres options de LO ici */}
          </Form.Control>
          <Button type="button" onClick={handleLOAdd}>
            Ajouter
          </Button>
          {duplicateError && (
            <Alert variant="danger" className="mt-2">
              Ce LO est déjà sélectionné.
            </Alert>
          )}
        </Form.Group>
        <Form.Group controlId="selectedLOs">
          <Form.Label>LOs sélectionnés :</Form.Label>
          <ListGroup>
            {los.map((lo, index) => (
              <ListGroup.Item key={index}>
                {lo}
                <AiOutlineCloseCircle
                  style={{
                    cursor: "pointer",
                    marginLeft: "10px",
                    color: "red",
                  }}
                  onClick={() => handleLORemove(index)}
                />
              </ListGroup.Item>
            ))}
          </ListGroup>
        </Form.Group>
        <Button type="submit">Créer la formation</Button>
      </Form>
    </div>
  );
};

export default CreateFormation;
