import React from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import "./AdminSideBar.css";

const AdminSideBar = ({ activeComponent, handleComponentChange }) => {
  return (
    <Navbar
      bg="dark"
      variant="dark"
      expand="lg"
      className="navbar-dark full-height-navbar"
    >
      <Container fluid>
        <div className="navbar-align-container">
          <Navbar.Brand href="#">Admin</Navbar.Brand>
          <Nav className="flex-column">
            <Nav.Link
              href="#"
              active={activeComponent === "Dashboard"}
              onClick={() => handleComponentChange("Dashboard")}
            >
              Tableau de bord
            </Nav.Link>
            <Nav.Link
              href="#"
              active={activeComponent === "ManageUsers"}
              onClick={() => handleComponentChange("ManageUsers")}
            >
              Liste des utilisateurs
            </Nav.Link>
            <Nav.Link
              href="#"
              active={activeComponent === "CreateUser"}
              onClick={() => handleComponentChange("CreateUser")}
            >
              Ajouter un utilisateur
            </Nav.Link>
          </Nav>
        </div>
      </Container>
    </Navbar>
  );
};

export default AdminSideBar;
