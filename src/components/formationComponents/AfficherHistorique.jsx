import React from "react";
import { Graph } from "react-graph-vis";

const AfficherHistorique = ({ data }) => {
  // Convertir le tableau de données en un format compatible avec react-graph-vis
  const graphData = {
    nodes: data.map((node, index) => ({ id: index, label: node })),
    edges: [],
  };

  // Définir les options du graphique
  const graphOptions = {
    layout: {
      hierarchical: false,
    },
    edges: {
      color: "#000000",
    },
    // Ajoutez d'autres options ici...
  };

  return (
    <div>
      <h1>Graphique à partir du tableau</h1>
      <Graph
        graph={graphData}
        options={graphOptions}
        style={{ height: "400px" }}
      />
    </div>
  );
};

export default AfficherHistorique;
