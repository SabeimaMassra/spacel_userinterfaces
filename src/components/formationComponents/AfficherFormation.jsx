import React, { useContext, useEffect, useState } from "react";
import { Card, Modal, Button } from "react-bootstrap";
import Graph from "react-graph-vis";
import ToastContext from "../../contexts/ToastContext";
import $ from "jquery";
import { API_BASE_URL } from "../../config/config";
import QuizComponent from "./QuizComponent";
import { useNavigate } from "react-router-dom";
import ModalLOContent from "./ModalLoContent";
import "./css/AfficherFormation.css";
import { handlleLogout } from "../userFormComponents/utils/AuthUtils";
import { FaSignOutAlt } from "react-icons/fa";
import Auth from "../../contexts/Auth";

const AfficherFormation = ({
  listeLosSansPrerequis,
  listeLosNiveau,
  listeLosFormation,
  idTraining,
  resourcesList,
  setResourcesList,
  titleLo,
  formData,
}) => {
  console.log("f", formData);
  // Hooks d'état pour la navigation et la complétion de la formation
  const navigate = useNavigate();
  const [isTrainingCompleted, setIsTrainingCompleted] = useState(false);
  const [isSendTrainingCompleted, setIsSendTrainingCompleted] = useState(false);

  // Hooks d'état pour la gestion du modal
  const [showModal, setShowModal] = useState(false);
  const [modalContent, setModalContent] = useState({});

  // Hooks d'état pour la gestion du quiz
  const [showQuiz, setShowQuiz] = useState(false);
  const [quizLevel3Validated, setQuizLevel3Validated] = useState(false);
  const [quizFinalLevelValidated, setQuizFinalLevelValidated] = useState(false);
  const [counterAttempt, setCounterAttempt] = useState(1);

  // Hooks d'état pour la gestion des LOs validés
  const [validatedLOs, setValidatedLOs] = useState([]);

  // Contexte de toast
  const toast = useContext(ToastContext);

  // Hooks d'état pour la gestion du niveau actuel et du clic sur les ressources
  const [currentLevel, setCurrentLevel] = useState(1);
  const [isRessourceCliked, setIsRessourceCliked] = useState(false);

  // Variable pour le nombre de niveaux
  let numberOfLevels;

  const handleFinishClick = () => {
    navigate("/training");
  };

  const { setIsAuthenticated } = useContext(Auth);

  // =====fitltrage des ressources =======

  const getLoPrs = (UserObject, filteredResources) => {
    const _ = require("lodash");
    filteredResources = _.sortBy(
      filteredResources,

      [
        (Resource) => {
          return Resource.languagePedagogicalResource ===
            UserObject.preferredLangTraining
            ? 0
            : 1;
        },
        (Resource) => {
          return Resource.formatPedagogicalResource ===
            UserObject.preferredResourceFormat
            ? 0
            : 1;
        },
        (Resource) => {
          return Resource.levelPResource === UserObject.levelExp ? 0 : 1;
        },

        (Resource) => {
          return parseInt(Resource.durationPResource) <=
            UserObject.duartionPersonalizedLearningPath
            ? 0
            : 1;
        },
        "durationPResource",
      ]
    );

    return filteredResources;
  };

  // ===== END fitltrage des ressources =======

  // Fonction pour récupérer toutes les ressources pédagogiques
  function getAllPedagogicalResources() {
    const resources = JSON.parse(resourcesList);
    const filteredResources = getLoPrs(formData, resources); // Appliquer la fonction getLoPrs avec les ressources récupérées et l'objet UserObject contenant les critères de filtrage
    setResourcesList(filteredResources); // Mettre à jour l'état des ressources filtrées
  }

  useEffect(() => {
    // Appeler la fonction pour récupérer les ressources pédagogiques
    getAllPedagogicalResources();
    // eslint-disable-next-line
  }, []);

  // Création de la liste des LOs de niveau 1
  let los = listeLosSansPrerequis.map((item) => ({
    id: item.idcurrlevello,
    titre: item.idcurrlevello.replace("LO", "LO "),
    resources: item.resources,
    color: validatedLOs.includes(item.idcurrlevello) ? "#00ff00" : "#738cd5",
  }));

  // Création du mapping des prérequis pour chaque LO
  const loToPrereqs = {};
  listeLosNiveau.forEach((item) => {
    const lo = item.idcurrlevello;
    const prereq = item.idPreviouslevel;

    if (!loToPrereqs[lo]) {
      loToPrereqs[lo] = [];
    }

    if (!loToPrereqs[lo].includes(prereq)) {
      loToPrereqs[lo].push(prereq);
    }
  });

  for (let lo in loToPrereqs) {
    loToPrereqs[lo].sort();
  }

  // Création des niveaux avec leurs LOs et prérequis
  const levels = {};
  for (let lo in loToPrereqs) {
    const prereqs = loToPrereqs[lo];
    const key = prereqs.join(", ");

    if (!levels[key]) {
      levels[key] = {
        los: [],
        prerequis: prereqs.map((id) => id.replace("LO", "LO ")),
      };
    }

    levels[key].los.push({
      id: lo,
      titre: lo.replace("LO", "LO "),
      resources: [],
      color: validatedLOs.includes(lo) ? "#00ff00" : "#738cd5",
    });
  }

  // Création de la liste des niveaux
  const niveaux = Object.values(levels);

  const loInNiveaux = new Set();

  for (const niveau of listeLosNiveau) {
    loInNiveaux.add(niveau.idcurrlevello);
  }

  for (const lo of listeLosSansPrerequis) {
    loInNiveaux.add(lo.idcurrlevello);
  }

  // Ajout du dernier niveau avec les LOs sans prérequis
  const dernierNiveauLos = listeLosFormation.filter(
    (lo) => !loInNiveaux.has(lo.idcurrlevello)
  );

  const dernierNiveau = {
    los: dernierNiveauLos.map((lo) => ({
      id: lo.idcurrlevello,
      titre: lo.idcurrlevello,
      resources: lo.resources,
      color: validatedLOs.includes(lo.idcurrlevello) ? "#00ff00" : "#738cd5",
    })),
    prerequis: [],
  };

  niveaux.push(dernierNiveau);

  // Construction du parcours
  const parcours = {
    id: 1,
    titre: "parcours " + idTraining,
    niveaux: [
      {
        los,
      },
    ],
  };
  parcours.niveaux = parcours.niveaux.concat(niveaux);

  const cheminRouge = [];

  const options = {
    layout: {
      hierarchical: {
        direction: "UD",
        sortMethod: "directed",
      },
    },
    edges: {
      color: "#000000",
    },
    interaction: {
      zoomView: false,
      dragView: false,
      dragNodes: false,
    },
    height: "500px",
  };

  // Gestion de l'événement de clic sur un LO
  const handleResourceClick = (resource) => {
    setIsRessourceCliked(true);
    // Définition des données à envoyer
    var userData = JSON.parse(sessionStorage.getItem("userData"));

    const data = {
      trainingSelected: idTraining,
      idUser: userData.idUser,
      name: userData.name,
      learningObejct: resource.idLo,
      pedagoRes: resource.idPedagogicalResource,
      levelOfDifficultyOfResource: resource.levelPResource,
    };

    $.ajax({
      url: `${API_BASE_URL}/insertPath`,
      type: "POST",
      data: JSON.stringify(data),
      contentType: "application/json",
      success: function (response) {
        console.log("Success:", response);
      },
      error: function (xhr, status, error) {
        console.log("Error:", error);
      },
    });

    window.open(resource.urlPResource, "_blank");
  };

  // Création d'un mapping des ressources par LO
  const resourcesByLO = {};
  resourcesList.forEach((resource) => {
    if (!resourcesByLO[resource.idLo]) {
      resourcesByLO[resource.idLo] = [];
    }
    resourcesByLO[resource.idLo].push(resource);
  });

  // Gestion des événements sur le graphe
  const events = {
    click: function (event) {
      const { nodes } = event;
      if (nodes.length === 1) {
        const nodeID = nodes[0];
        const node = graph.nodes.find((node) => node.id === nodeID);

        if (node) {
          const lo = parcours.niveaux
            .map((niveau) => niveau.los)
            .flat()
            .find((lo) => lo.titre === node.label);

          const niveauIndex = parcours.niveaux.findIndex((niveau) =>
            niveau.los.includes(lo)
          );

          const prerequisComplete =
            niveauIndex === 0 ||
            parcours.niveaux[niveauIndex - 1].los.some((lo) =>
              validatedLOs.includes(lo.id)
            );

          if (!prerequisComplete) {
            toast.error(
              "Au moins l'un des prérequis pour ce LO doit être validé !"
            );
            return;
          }

          if (lo) {
            // Ajouter les ressources au contenu du modal
            setModalContent({
              ...lo,
              resources: resourcesByLO[lo.id] || [],
              niveau: node.niveau,
              titleLo: titleLo,
            });
            setShowModal(true);
          }
        }
      }
    },
  };

  // Validation d'un LO
  const validateLO = (lo) => {
    if (!isRessourceCliked) {
      toast.error(
        "Vous devez visiter au moins une ressource avant de valider un LO."
      );
    } else {
      if (quizFinalLevelValidated && quizLevel3Validated) {
        setValidatedLOs((prevLOs) => [...prevLOs, lo.id]);
        toast.success("Vous avez bien validé le LO.");
        TrainingCompleted();
      } else if (lo.niveau < currentLevel) {
        setValidatedLOs((prevLOs) => [...prevLOs, lo.id]);
        toast.success(
          "Vous avez bien validé le LO. Vous pouvez passer au suivant."
        );
      } else {
        if (!quizLevel3Validated && lo.niveau === 3) {
          setShowQuiz(true);
        } else if (lo.niveau === numberOfLevels && !quizFinalLevelValidated) {
          setShowQuiz(true);
        } else {
          setValidatedLOs((prevLOs) => [...prevLOs, lo.id]);
          setCurrentLevel((prevLevel) => prevLevel + 1);
          toast.success(
            "Vous avez bien validé le LO. Vous pouvez passer au suivant."
          );
        }
        setIsRessourceCliked(false);
        setShowModal(false);
      }
    }

    console.log("niveau actuel:", currentLevel, "niveau total", numberOfLevels);
  };

  const TrainingCompleted = () => {
    if (
      currentLevel === numberOfLevels ||
      (quizFinalLevelValidated && quizLevel3Validated)
    ) {
      setIsTrainingCompleted(true);
      if (!isSendTrainingCompleted) {
        SendTrainingCompleted();
      }
    }
  };

  const SendTrainingCompleted = () => {
    setIsSendTrainingCompleted(true);
  };

  const graph = {
    nodes: [],
    edges: [],
  };

  let nodeId = 0;
  let edgeId = 0;
  let niveauID = 0;
  // Construction du graphe
  parcours.niveaux.forEach((niveau, niveauIndex) => {
    niveauID++;
    numberOfLevels = niveauID;
    niveau.los.forEach((lo) => {
      const nodeID = `node-${parcours.id}-${nodeId++}`;
      graph.nodes.push({
        id: nodeID,
        label: lo.titre,
        title: lo.titre,
        color: lo.color,
        niveau: niveauID,
      });

      if (niveauIndex > 0) {
        const prerequis = parcours.niveaux[niveauIndex - 1].los;

        prerequis.forEach((prerequisLo) => {
          const prerequisNodeId = graph.nodes.find(
            (node) => node.label === prerequisLo.titre
          ).id;

          const isRedPath =
            cheminRouge.includes(prerequisLo.titre) &&
            cheminRouge.includes(lo.titre);

          graph.edges.push({
            id: `edge-${parcours.id}-${edgeId++}`,
            from: prerequisNodeId,
            to: nodeID,
            color: isRedPath ? "red" : "#000000",
          });
        });
      }
    });
  });

  function getRandomNumber() {
    return Math.floor(Math.random() * 1000); // Génère un nombre aléatoire entre 0 et 999
  }

  function insertQuizData(idAssessment, score) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    var username = userData.name;

    console.log("idAssessment :" + idAssessment);
    console.log("passed :" + true);
    console.log("nombre essai : " + counterAttempt);
    console.log("levelAssessment : " + currentLevel);
    console.log("assement : " + getRandomNumber());
    console.log(" Quiz niveau" + currentLevel);
    $.ajax({
      async: true,
      crossDomain: true,
      url: `${API_BASE_URL}/insertassesmentresult`,
      type: "POST",
      data: {
        name: username,
        idUser: userData.idUser,
        idAssessment: "Assessment" + idAssessment,
        passedAssessment: true,
        numberAttempt: counterAttempt,
        levelAssessment: currentLevel,
        Assessment: getRandomNumber(),
        titleAssessment: "Quiz niveau" + currentLevel,
        result: score,
      },
      success: function (response) {
        alert("insertion des donnés du quiz ");
        console.log(response);
      },
      error: function (xhr, status, error) {
        console.log(error);
      },
    });
  }
  function getTitleById(id) {
    const formattedId = id.replace("LO ", "LO");
    const learningObject = titleLo.find(
      (obj) => obj.idcurrlevello === formattedId
    );
    return learningObject ? learningObject.titleLearningObject : null;
  }
  return (
    <div className="container-wrapper">
      <div className="container-content">
        <h1 className="text-center mb-4">{parcours.titre}</h1>
        <Card>
          <Card.Body>
            <div className="card-container">
              <div className="graph-card">
                <Graph graph={graph} options={options} events={events} />
              </div>
              <div className="info-card">
                <Card>
                  <Card.Body>
                    <h4 className="mb-4">Informations du graphe :</h4>
                    <p>Titre du parcours : {parcours.titre}</p>
                    <p>Nombre de niveaux : {numberOfLevels}</p>

                    {/* Afficher les niveaux et les LOs */}
                    {parcours.niveaux.map((niveau, index) => (
                      <div className="niveau" key={index}>
                        <h5>
                          <i className="bi bi-chevron-right me-2"></i>
                          Niveau {index + 1}
                        </h5>
                        <ul className="list-group">
                          {niveau.los.map((lo) => (
                            <li
                              className={`list-group-item ${
                                validatedLOs.includes(lo.id)
                                  ? "validated-lo"
                                  : "not-validated-lo"
                              }`}
                              key={lo.id}>
                              {lo.titre} : {getTitleById(lo.id)}
                            </li>
                          ))}
                        </ul>
                      </div>
                    ))}
                  </Card.Body>
                </Card>
              </div>
            </div>
          </Card.Body>
        </Card>

        {isTrainingCompleted ? (
          <Modal show={true} onHide={() => setIsTrainingCompleted(false)}>
            <Modal.Header closeButton>
              <Modal.Title>Formation terminée</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p>
                Vous avez terminé la formation. Que souhaitez-vous faire ensuite
                ?
              </p>
            </Modal.Body>
            <Modal.Footer>
              <div
                className="logout-button"
                onClick={() => {
                  handlleLogout(toast, setIsAuthenticated, navigate);
                }}>
                <FaSignOutAlt className="logout-button-icon" />
                Déconnexion
              </div>
              <Button variant="primary" onClick={handleFinishClick}>
                Faire une autre formation
              </Button>
            </Modal.Footer>
          </Modal>
        ) : (
          <Modal show={showModal} onHide={() => setShowModal(false)} size="lg">
            <ModalLOContent
              modalContent={modalContent}
              handleResourceClick={handleResourceClick}
              validateLO={validateLO}
            />
          </Modal>
        )}
        {showQuiz ? (
          <QuizComponent
            level={currentLevel}
            counterAttempt={counterAttempt}
            setCounterAttempt={() => setCounterAttempt(counterAttempt + 1)}
            onClose={() => setShowQuiz(false)}
            onQuizSubmit={(score) => {
              setCounterAttempt(counterAttempt + 1);
              setShowQuiz(false);
              setValidatedLOs((prevLOs) => [...prevLOs, modalContent.id]);
              toast.success("Vous avez validé le LO avec succès.");
              if (currentLevel === 3) {
                setQuizLevel3Validated(true);
                insertQuizData("1", score);
              }
              if (currentLevel === numberOfLevels) {
                setQuizFinalLevelValidated(true);
                insertQuizData("2", score);
              }

              setCounterAttempt(1);
              setCurrentLevel((prevLevel) => prevLevel + 1);
              TrainingCompleted();
            }}
          />
        ) : null}
      </div>
    </div>
  );
};

export default AfficherFormation;
