import React from "react";
import { Modal, ListGroup, Button } from "react-bootstrap";
import {
  FaRegIdBadge,
  FaLanguage,
  FaRegFileAlt,
  FaRegClock,
  FaStar,
} from "react-icons/fa";

import YouTube from "react-youtube";

const ModalLoContent = ({
  modalContent,
  handleResourceClick,
  validateLO,
  historique,
  setShowModal,
}) => {
  function getTitleById(id) {
    const formattedId = id.replace("LO ", "LO");
    const learningObject = modalContent.titleLo.find(
      (obj) => obj.idcurrlevello === formattedId
    );
    return learningObject ? learningObject.titleLearningObject : null;
  }

  function formatDateTime(dateTime) {
    const parts = dateTime.split(",");
    const datePart = parts[0].trim();
    const timePart = parts[1].trim();
    return `${datePart} à ${timePart.substring(0, 8)}`;
  }

  const isYouTubeVideo = (url) => {
    const parsedUrl = new URL(url);
    return ["www.youtube.com", "youtu.be"].includes(parsedUrl.hostname);
  };

  const getYouTubeVideoId = (url) => {
    const parsedUrl = new URL(url);
    if (parsedUrl.hostname === "www.youtube.com") {
      return parsedUrl.searchParams.get("v");
    } else if (parsedUrl.hostname === "youtu.be") {
      return parsedUrl.pathname.slice(1);
    }
  };

  const isMobile = () => {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      window.navigator.userAgent
    );
  };

  const renderContent = () => {
    if (isYouTubeVideo(modalContent.resource.urlPResource) && !isMobile()) {
      const videoId = getYouTubeVideoId(modalContent.resource.urlPResource);
      return <YouTube videoId={videoId} />;
    } else {
      return (
        <a
          href={modalContent.resource.urlPResource}
          className="btn btn-secondary mt-3"
          target="blank">
          Accéder à la ressource
        </a>
      );
    }
  };

  const LANGUAGES = {
    "": "Choisir...",
    en: "Anglais",
    fr: "Français",
    ar: "Arabe",
    es: "Espagnol",
    de: "Allemand",
    it: "Italien",
    ru: "Russe",
    pt: "Portugais",
    zh: "Chinois",
    ja: "Japonais",
    ko: "Coréen",
    hi: "Hindi",
  };

  function getLanguageFull(languageCode) {
    return LANGUAGES[languageCode] || languageCode;
  }

  function getFormatFull(format) {
    switch (format) {
      case "V":
        return "Vidéo";
      case "T":
        return "Texte";
      case "A":
        return "Audio";
      // Ajoutez d'autres cas si nécessaire
      default:
        return format;
    }
  }

  if (!historique) {
    return (
      <>
        <Modal.Header closeButton>
          <Modal.Title>
            <b>
              {getTitleById(modalContent.titre)} niveau :{modalContent.niveau}
            </b>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ListGroup variant="flush">
            {modalContent.resources &&
              modalContent.resources.map((resource, index) => (
                <ListGroup.Item
                  key={index}
                  style={{ cursor: "pointer" }}
                  onClick={() => handleResourceClick(resource)}>
                  <h5>
                    <b style={{ color: "blue" }}>
                      {resource.titlePedagogicalResource}
                    </b>
                  </h5>
                  <p>
                    <FaRegIdBadge color="gray" /> <b>ID:</b>{" "}
                    {resource.idPedagogicalResource} <br />
                    <FaLanguage color="gray" /> <b>Langue:</b>{" "}
                    {getLanguageFull(resource.languagePedagogicalResource)}{" "}
                    <br />
                    <FaRegFileAlt color="gray" /> <b>Format:</b>{" "}
                    {getFormatFull(resource.formatPedagogicalResource)} <br />
                    <b>Niveau de difficulté:</b>{" "}
                    {resource.levelPResource >= 1 && <FaStar color="gray" />}
                    {resource.levelPResource >= 2 && <FaStar color="gray" />}
                    {resource.levelPResource >= 3 && <FaStar color="gray" />}
                    <br />
                    <FaRegClock color="gray" /> <b>Durée:</b>{" "}
                    {resource.durationPResource + " minutes"}
                  </p>
                </ListGroup.Item>
              ))}
          </ListGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => validateLO(modalContent)} variant="success">
            Valider le LO
          </Button>
        </Modal.Footer>
      </>
    );
  } else {
    return (
      <>
        <Modal.Header closeButton className="bg-secondary text-white">
          <Modal.Title>
            <b>
              <FaRegIdBadge /> Voici la ressource que vous aviez visité lors de
              votre parcours de {modalContent.training} le{" "}
              {formatDateTime(modalContent.date)}
            </b>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="d-flex flex-column align-items-start">
            <p>
              <FaRegIdBadge color="gray" /> <b>ID:</b>{" "}
              {modalContent.resource.idPedagogicalResource} <br />
              <FaLanguage color="gray" /> <b>Langue:</b>{" "}
              {getLanguageFull(
                modalContent.resource.languagePedagogicalResource
              )}{" "}
              <br />
              <FaRegFileAlt color="gray" /> <b>Format:</b>{" "}
              {getFormatFull(modalContent.resource.formatPedagogicalResource)}{" "}
              <br />
              <b>Niveau de difficulté:</b>
              {Array(modalContent.resource.levelPResource)
                .fill(0)
                .map((_, i) => (
                  <FaStar color="gray" key={i} />
                ))}
              <br />
              <FaRegClock color="gray" /> <b>Durée:</b>{" "}
              {modalContent.resource.durationPResource + " minutes"}
            </p>
            {renderContent()}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => setShowModal(false)}>
            Fermer le modal
          </Button>
        </Modal.Footer>
      </>
    );
  }
};

export default ModalLoContent;
