import React, { useContext, useState, useCallback } from "react";
import Modal from "react-modal";
import "./css/QuizComponent.css";
import ToastContext from "../../contexts/ToastContext";
import $ from "jquery";
import { API_BASE_URL } from "../../config/config";
const QuizComponent = ({
  onClose,
  onQuizSubmit,
  level,
  counterAttempt,
  setCounterAttempt,
}) => {
  var questions;
  if (level === 3) {
    questions = [
      {
        question:
          "Quelle est la valeur de x après l'exécution de ce code ?\n\nint x = 10;\nx += 5;\nx *= 2;",
        options: ["25", "30", "35", "40"],
        correctAnswer: 1,
      },
      {
        question:
          "Quelle est la sortie du code suivant ?\n\nint x = 10;\nint y = 5;\nSystem.out.println(x > y);",
        options: ["True", "False"],
        correctAnswer: 0,
      },
      {
        question:
          "Quel est le type de données approprié pour stocker le nombre décimal 3.14 en Java ?",
        options: ["int", "double", "float", "String"],
        correctAnswer: 1,
      },
      {
        question:
          "Quelle est la sortie de ce code ?\n\nint num1 = 5;\nint num2 = 2;\ndouble result = (double) num1 / num2;\nSystem.out.println(result);",
        options: ["2", "2.5", "3", "3.5"],
        correctAnswer: 1,
      },
      {
        question:
          "Quelle est la valeur de la variable 'total' après l'exécution de ce code ?\n\nint total = 10;\ntotal++;\ntotal += 5;\ntotal *= 2;\ntotal -= 3;",
        options: ["21", "26", "29", "32"],
        correctAnswer: 2,
      },
    ];
  } else {
    questions = [
      {
        question:
          "Quel mot clé est utilisé pour définir une instruction conditionnelle en Java ?",
        options: ["for", "if", "while", "switch"],
        correctAnswer: 1,
      },
      {
        question:
          "Quelle instruction est utilisée pour sortir prématurément d'une boucle ?",
        options: ["continue", "break", "return", "exit"],
        correctAnswer: 1,
      },
      {
        question: "Quel est le rôle de l'instruction else ?",
        options: [
          "Spécifier un ensemble d'instructions alternatif à exécuter si la condition de l'instruction if est fausse.",
          "Terminer l'exécution du programme.",
          "Répéter un ensemble d'instructions jusqu'à ce qu'une condition devienne fausse.",
          "Vérifier plusieurs conditions séquentiellement.",
        ],
        correctAnswer: 0,
      },
      {
        question:
          "Quelle est la différence entre les opérateurs logiques && et || en Java ?",
        options: [
          "&& est l'opérateur de conjonction logique, tandis que || est l'opérateur de disjonction logique.",
          "|| est l'opérateur de conjonction logique, tandis que && est l'opérateur de disjonction logique.",
          "&& et || effectuent la même opération de comparaison logique.",
          "&& et || sont utilisés pour effectuer des opérations mathématiques.",
        ],
        correctAnswer: 0,
      },
      {
        question:
          "Quelle est la syntaxe correcte pour déclarer une variable de type String appelée 'nom' et lui attribuer la valeur 'John' ?",
        options: [
          "String nom = 'John';",
          "String = nom 'John';",
          "nom = String 'John';",
          "String = 'John' nom;",
        ],
        correctAnswer: 0,
      },
      {
        question:
          "Quelle est la sortie du code suivant ?\n\nString str = 'Hello';\nSystem.out.println(str.length());",
        options: ["4", "5", "6", "7"],
        correctAnswer: 1,
      },
      {
        question:
          "Quelle est la valeur de la variable 'result' après l'exécution de ce code ?\n\nint num = 7;\nint result = num > 5 ? 10 : 5;",
        options: ["5", "7", "10", "Error"],
        correctAnswer: 2,
      },
      {
        question:
          "Quelle est la différence entre '==' et '.equals()' lors de la comparaison de deux objets String ?",
        options: [
          "'==' compare les références d'objets, tandis que '.equals()' compare le contenu des objets String.",
          "'==' compare le contenu des objets String, tandis que '.equals()' compare les références d'objets.",
          "'==' et '.equals()' effectuent la même opération de comparaison pour les objets String.",
          "'==' et '.equals()' sont utilisés pour la comparaison d'objets de types différents, pas pour les objets String.",
        ],
        correctAnswer: 0,
      },
    ];
  }
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [selectedAnswer, setSelectedAnswer] = useState(null);
  const [submitted, setSubmitted] = useState(false);
  const [quizRecap, setQuizRecap] = useState([]);
  const toast = useContext(ToastContext);

  const handleAnswerSelect = (optionIndex) => {
    setSelectedAnswer(optionIndex);
  };

  const handleNextQuestion = () => {
    if (selectedAnswer !== null) {
      const nextRecap = [
        ...quizRecap,
        { question: questions[currentQuestionIndex], answer: selectedAnswer },
      ];
      setQuizRecap(nextRecap);
      if (currentQuestionIndex < questions.length - 1) {
        setCurrentQuestionIndex((prevIndex) => prevIndex + 1);
        setSelectedAnswer(null);
      } else {
        setSubmitted(true);
      }
    }
  };

  const calculateCorrectAnswers = useCallback(() => {
    return quizRecap.reduce(
      (count, item) =>
        item.answer === item.question.correctAnswer ? count + 1 : count,
      0
    );
  }, [quizRecap]);

  const handleQuizSubmit = () => {
    const isQuizPassed = calculateCorrectAnswers() === questions.length;
    if (isQuizPassed) {
      onQuizSubmit(calculateCorrectAnswers() + "/" + questions.length);
    } else {
      onClose();

      if (level === 3) {
        insertQuizData("1");
      } else {
        insertQuizData("2");
      }
      setCounterAttempt(counterAttempt + 1);

      toast.error(
        "Pour passer au niveau suivant , vous devez réussir le test avec succès."
      );
      toast.error("Vous pouvez revoir d'autres ressources ou refaire le test");
    }
  };
  function getRandomNumber() {
    return Math.floor(Math.random() * 1000); // Génère un nombre aléatoire entre 0 et 999
  }
  function insertQuizData(idAssessment) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    var username = userData.name;

    console.log("idAssessment :" + idAssessment);
    console.log("passed : " + false);
    console.log("nombre essai : " + counterAttempt);
    console.log("levelAssessment : " + level);
    console.log("assement : " + getRandomNumber());
    console.log(" Quiz niveau" + level);
    $.ajax({
      async: true,
      crossDomain: true,
      url: `${API_BASE_URL}/insertassesmentresult`,
      type: "POST",
      data: {
        name: username,
        idUser: userData.idUser,
        idAssessment: "Assessment" + idAssessment,
        passedAssessment: false,
        numberAttempt: counterAttempt,
        levelAssessment: level,
        Assessment: getRandomNumber(),
        titleAssessment: "Quiz niveau" + level,
        result: calculateCorrectAnswers() + "/" + questions.length,
      },
      success: function (response) {
        console.log(response);
      },
      error: function (xhr, status, error) {
        console.log(error);
      },
    });
  }

  return (
    <Modal isOpen={true} onRequestClose={onClose}>
      <div className="quiz-container">
        {submitted ? (
          <div className="recap-container">
            <h2>Récapitulatif du Quiz</h2>
            <div>
              {quizRecap.map((item, index) => (
                <div
                  key={index}
                  className={`recap-question ${
                    item.answer !== item.question.correctAnswer
                      ? "incorrect"
                      : "correct"
                  }`}>
                  <h3>Question {index + 1}</h3>
                  <p>{item.question.question}</p>
                  <p>
                    Réponse sélectionnée : {item.question.options[item.answer]}
                  </p>
                </div>
              ))}
            </div>
            <p className="result">
              Nombre de bonnes réponses : {calculateCorrectAnswers()}
            </p>
            <div className="button-container">
              <button className="send-quiz-button" onClick={handleQuizSubmit}>
                Envoyer le Quiz
              </button>
            </div>
          </div>
        ) : (
          <div>
            <h2>Quiz sur Java</h2>
            <div className="question">
              <p>
                Question {currentQuestionIndex + 1} sur {questions.length}
              </p>
              <p>{questions[currentQuestionIndex].question}</p>
            </div>
            <div className="options">
              {questions[currentQuestionIndex].options.map(
                (option, optionIndex) => (
                  <button
                    key={optionIndex}
                    className={`option-button ${
                      selectedAnswer === optionIndex ? "selected" : ""
                    }`}
                    onClick={() => handleAnswerSelect(optionIndex)}>
                    {option}
                  </button>
                )
              )}
            </div>
            {selectedAnswer !== null && (
              <div className="button-container">
                <button className="quiz-button" onClick={handleNextQuestion}>
                  {currentQuestionIndex === questions.length - 1
                    ? "Soumettre le quiz"
                    : "Continuer"}
                </button>
              </div>
            )}
          </div>
        )}
        <div className="button-container">
          <button className="close-button" onClick={onClose}>
            Fermer
          </button>
        </div>
      </div>
    </Modal>
  );
};

export default QuizComponent;
