import React, { useContext, useState } from "react";
import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap";
import Auth from "../../contexts/Auth";
import ToastContext from "../../contexts/ToastContext";
import { Link, useNavigate } from "react-router-dom";
import { FaSignOutAlt } from "react-icons/fa";
import "./navbar.css"; // Import custom CSS file
import { handlleLogout } from "../userFormComponents/utils/AuthUtils";
function AuthNavBar() {
  const { isAuthenticated, setIsAuthenticated } = useContext(Auth);
  const toast = useContext(ToastContext);
  const [expanded, setExpanded] = useState(false);
  const navigate = useNavigate();
  var username = "";
  if (isAuthenticated) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    username = userData.name;
  }

  const handleLogout = () => {
    handlleLogout(toast, setIsAuthenticated, navigate);
  };

  return (
    <Navbar
      bg="dark"
      variant="dark"
      expand="lg"
      className="navbar-custom"
      expanded={expanded}>
      <Container fluid>
        <Navbar.Brand as={Link} to="/">
          Spac-El
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="navbarScroll"
          onClick={() => setExpanded(!expanded)}
        />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "90px" }}
            navbarScroll>
            <Nav.Link as={Link} to="/">
              Portfolio
            </Nav.Link>
            <Nav.Link as={Link} to="/">
              Editeur d'annotation
            </Nav.Link>
            {isAuthenticated ? (
              <Nav.Link as={Link} to="/training">
                Trouvez votre formation
              </Nav.Link>
            ) : (
              <span></span>
            )}
          </Nav>
          <Nav className="ml-auto">
            {isAuthenticated ? (
              <>
                <Nav.Link></Nav.Link>
                <Nav.Link as={Link} to="/historiqueParcours">
                  Visualiser mes formations terminées
                </Nav.Link>
                <NavDropdown
                  title={username}
                  id="user-dropdown"
                  align="end"
                  menuVariant="dark"
                  className="dropdown-menu-right custom-dropdown">
                  <Link to="/ProfilPage" style={{ color: "#000" }}>
                    Gérer mon profil
                  </Link>
                  <NavDropdown.Divider />
                  <Link to="/historiqueParcours" style={{ color: "#000" }}>
                    formations terminées
                  </Link>
                </NavDropdown>
                <div className="logout-button" onClick={handleLogout}>
                  <FaSignOutAlt className="logout-button-icon" />
                  Déconnexion
                </div>
              </>
            ) : (
              <Nav.Link as={Link} to="/AuthentificationForm">
                Connexion/Inscription
              </Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AuthNavBar;
