import React, { useState } from "react";
import { Button, ListGroup, Form, Alert, Row, Col } from "react-bootstrap";
import { FaUser, FaCog, FaCheck } from "react-icons/fa";
import "./css/RecapForm.css";

function RecapForm({ formData, prevStep, handleSubmit, onSigninClick }) {
  const [acceptConditions, setAcceptConditions] = useState(false);
  const [showError, setShowError] = useState(false);

  const handleCheckboxChange = (event) => {
    setAcceptConditions(event.target.checked);
  };

  const handleFormSubmit = () => {
    if (acceptConditions) {
      handleSubmit();
      setShowError(false);
    } else {
      setShowError(true);
    }
  };

  return (
    <div className="recap-form-container">
      <h2>Récapitulatif :</h2>
      <Row>
        <Col>
          <div className="category">
            <div className="category-icon">
              <FaUser />
            </div>
            <div className="category-title">Détails personnels</div>
            <ListGroup>
              <ListGroup.Item>
                <strong>Nom :</strong> {formData.lastName}
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Prénom :</strong> {formData.firstName}
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Genre :</strong> {formData.gender}
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Date de naissance :</strong> {formData.dateOfBirth}
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Email :</strong> {formData.email}
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Nom d'utilisateur :</strong> {formData.username}
              </ListGroup.Item>
            </ListGroup>
          </div>
        </Col>
        <Col>
          <div className="category">
            <div className="category-icon">
              <FaCog />
            </div>
            <div className="category-title">Préférences d'apprentissage</div>
            <ListGroup>
              <ListGroup.Item>
                <strong>Mode d'apprentissage collectif préféré :</strong>{" "}
                {formData.PreferredCollectiveLearningMode}%
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Mode d'apprentissage individuel préféré :</strong>{" "}
                {formData.PreferredIndividualLearningMode}%
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Format audio préféré :</strong>{" "}
                {formData.PreferredFormatAudio}%
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Format texte préféré :</strong>{" "}
                {formData.PreferredFormatText}%
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Format vidéo préféré :</strong>{" "}
                {formData.PreferredFormatVideo}%
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Langue préférée :</strong> {formData.preferredLanguage1}
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Deuxième langue préférée :</strong>{" "}
                {formData.preferredLanguage2}
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Troisième langue préférée :</strong>{" "}
                {formData.preferredLanguage3}
              </ListGroup.Item>
            </ListGroup>
          </div>
        </Col>
      </Row>

      <Form.Group controlId="formBasicCheckbox">
        <Form.Check
          type="checkbox"
          label="J'accepte les conditions d'utilisation"
          checked={acceptConditions}
          onChange={handleCheckboxChange}
        />
      </Form.Group>
      {showError && (
        <Alert variant="danger">
          Veuillez accepter les conditions d'utilisation pour continuer.
        </Alert>
      )}
      <div className="buttons-container">
        <Button variant="secondary" onClick={prevStep}>
          Précédent
        </Button>
        <Button variant="primary" type="submit" onClick={handleFormSubmit}>
          <FaCheck /> Soumettre
        </Button>
      </div>
      <div className="signin-link">
        Vous possédez déjà un compte?{" "}
        <button className="signin-button" onClick={onSigninClick}>
          Connectez-vous
        </button>
      </div>
    </div>
  );
}

export default RecapForm;
