export const handlleLogout = (toast, setIsAuthenticated, navigate) => {
  sessionStorage.removeItem("isAuthenticated");
  setIsAuthenticated(false);
  toast.info("Vous avez été déconnecté !");
  navigate("/AuthentificationForm");
};
