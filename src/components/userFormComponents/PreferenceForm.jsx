import React, { useContext, useState } from "react";
import {
  Form,
  Button,
  Row,
  Col,
  Alert,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import ToastContext from "../../contexts/ToastContext";
import "./css/PreferenceForm.css"; // Fichier de styles CSS personnalisés
import { FaInfoCircle } from "react-icons/fa";

const LANGUAGES = [
  { code: "", label: "Choisir..." },
  { code: "en", label: "Anglais" },
  { code: "fr", label: "Français" },
  { code: "ar", label: "Arabe" },
  { code: "es", label: "Espagnol" },
  { code: "de", label: "Allemand" },
  { code: "it", label: "Italien" },
  { code: "ru", label: "Russe" },
  { code: "pt", label: "Portugais" },
  { code: "zh", label: "Chinois" },
  { code: "ja", label: "Japonais" },
  { code: "ko", label: "Coréen" },
  { code: "hi", label: "Hindi" },
];

function PreferenceForm({
  formData,
  setFormData,
  prevStep,
  nextStep,
  onSigninClick,
}) {
  const [usernameError, setUsernameError] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const toast = useContext(ToastContext);

  const handleChange = (event) => {
    const { name, value } = event.target;

    if (
      (name === "preferredLanguage1" &&
        (value === formData.preferredLanguage2 ||
          value === formData.preferredLanguage3)) ||
      (name === "preferredLanguage2" &&
        (value === formData.preferredLanguage1 ||
          value === formData.preferredLanguage3)) ||
      (name === "preferredLanguage3" &&
        (value === formData.preferredLanguage1 ||
          value === formData.preferredLanguage2))
    ) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        [name]: "",
      }));
      toast.warn(
        "Vous avez déjà sélectionné cette langue, veuillez en choisir une autre."
      );
    } else {
      setFormData((prevFormData) => ({
        ...prevFormData,
        [name]: value,
      }));
    }

    if (name === "username") {
      setUsernameError("");
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const isValid = isUsernameValid(formData.username.trim());

    if (isValid && isCursorValid()) {
      nextStep();
      return;
    }
    return;
  };

  const isCursorValid = () => {
    if (
      formData.PreferredCollectiveLearningMode !== "" &&
      formData.PreferredIndividualLearningMode !== "" &&
      formData.PreferredFormatAudio !== "" &&
      formData.PreferredFormatText !== "" &&
      formData.PreferredFormatVideo !== ""
    ) {
      return true;
    } else {
      setShowAlert(true);
      return false;
    }
  };

  const isUsernameValid = (username) => {
    const usernameRegex = /^[a-zA-Z0-9_-]{3,16}$/;
    if (!usernameRegex.test(username) || username.trim() === "") {
      setUsernameError("Veuillez saisir un nom d'utilisateur valide.");
      return false;
    } else {
      setUsernameError("");
      return true;
    }
  };

  return (
    <div className="preference-form-container">
      <h3 className="form-title">Préférences :</h3>
      <Form className="preference-form" onSubmit={handleSubmit}>
        <Form.Group as={Row} controlId="formUsername">
          <Form.Label column sm={3}>
            Nom d'utilisateur
          </Form.Label>
          <Col sm={9}>
            <Form.Control
              type="text"
              name="username"
              placeholder="Nom d'utilisateur"
              onChange={handleChange}
              required
              isInvalid={usernameError !== ""}
              value={formData.username}
            />
            <Form.Control.Feedback type="invalid">
              {usernameError}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formPreferredLanguage">
          <Form.Label column sm={3}>
            Langue préférée
            <OverlayTrigger
              placement="right"
              overlay={
                <Tooltip>
                  Sélectionnez votre langue préférée pour les ressources pendant
                  votre parcours d'apprentissage
                </Tooltip>
              }>
              <span className="info-icon">
                <FaInfoCircle />
              </span>
            </OverlayTrigger>
          </Form.Label>
          <Col sm={9}>
            <Form.Control
              as="select"
              name="preferredLanguage1"
              value={formData.preferredLanguage1}
              onChange={handleChange}
              required>
              {LANGUAGES.map((language) => (
                <option key={language.code} value={language.code}>
                  {language.label}
                </option>
              ))}
            </Form.Control>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formSecondLanguage">
          <Form.Label column sm={3}>
            Deuxième langue préférée
          </Form.Label>
          <Col sm={9}>
            <Form.Control
              as="select"
              name="preferredLanguage2"
              value={formData.preferredLanguage2}
              onChange={handleChange}
              required>
              {LANGUAGES.map((language) => (
                <option key={language.code} value={language.code}>
                  {language.label}
                </option>
              ))}
            </Form.Control>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formThirdLanguage">
          <Form.Label column sm={3}>
            Troisième langue préférée
          </Form.Label>
          <Col sm={9}>
            <Form.Control
              as="select"
              name="preferredLanguage3"
              value={formData.preferredLanguage3}
              onChange={handleChange}
              required>
              {LANGUAGES.map((language) => (
                <option key={language.code} value={language.code}>
                  {language.label}
                </option>
              ))}
            </Form.Control>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formPreferredCollectiveLearningMode">
          <Form.Label column sm={3}>
            Mode d'apprentissage collectif préféré:{" "}
            {formData.PreferredCollectiveLearningMode}%
            <OverlayTrigger
              placement="right"
              overlay={
                <Tooltip>
                  Indiquez votre préférence d'apprendre de manière collective
                </Tooltip>
              }>
              <span className="info-icon">
                <FaInfoCircle />
              </span>
            </OverlayTrigger>
          </Form.Label>
          <Col sm={9}>
            <Form.Control
              type="range"
              min="0"
              max="100"
              step="10"
              required
              name="PreferredCollectiveLearningMode"
              onChange={handleChange}
              value={formData.PreferredCollectiveLearningMode}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formPreferredIndividualLearningMode">
          <Form.Label column sm={3}>
            Mode d'apprentissage individuel préféré:{" "}
            {formData.PreferredIndividualLearningMode}%
            <OverlayTrigger
              placement="right"
              overlay={
                <Tooltip>
                  Indiquez votre préférence d'apprendre de manière individuel
                </Tooltip>
              }>
              <span className="info-icon">
                <FaInfoCircle />
              </span>
            </OverlayTrigger>
          </Form.Label>
          <Col sm={9}>
            <Form.Control
              type="range"
              min="0"
              max="100"
              step="10"
              required
              name="PreferredIndividualLearningMode"
              onChange={handleChange}
              value={formData.PreferredIndividualLearningMode}
            />
          </Col>
        </Form.Group>
        <OverlayTrigger
          placement="right"
          overlay={
            <Tooltip>
              Indiquez votre préférence de format pour les ressources pendant
              votre parcours d'apprentissage
            </Tooltip>
          }>
          <span className="info-icon">
            <FaInfoCircle />
          </span>
        </OverlayTrigger>
        <Form.Group as={Row} controlId="formPreferredFormatAudio">
          <Form.Label column sm={3}>
            Format audio préféré: {formData.PreferredFormatAudio}%
          </Form.Label>
          <Col sm={9}>
            <Form.Control
              type="range"
              min="0"
              max="100"
              step="10"
              required
              name="PreferredFormatAudio"
              onChange={handleChange}
              value={formData.PreferredFormatAudio}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formPreferredFormatText">
          <Form.Label column sm={3}>
            Format texte préféré: {formData.PreferredFormatText}%
          </Form.Label>
          <Col sm={9}>
            <Form.Control
              type="range"
              min="0"
              max="100"
              step="10"
              required
              name="PreferredFormatText"
              onChange={handleChange}
              value={formData.PreferredFormatText}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formPreferredFormatVideo">
          <Form.Label column sm={3}>
            Format vidéo préféré: {formData.PreferredFormatVideo}%
          </Form.Label>
          <Col sm={9}>
            <Form.Control
              type="range"
              min="0"
              max="100"
              step="10"
              required
              name="PreferredFormatVideo"
              onChange={handleChange}
              value={formData.PreferredFormatVideo}
            />
          </Col>
        </Form.Group>

        {showAlert && (
          <Alert
            variant="danger"
            onClose={() => setShowAlert(false)}
            dismissible>
            <p className="alert-message">
              Veuillez interagir avec tous les curseurs avant de continuer.
            </p>
          </Alert>
        )}

        <hr />
        <Row className="m-0">
          <Button
            variant="secondary"
            onClick={prevStep}
            className="btn-block align-self-center">
            Précédent
          </Button>
          <Button
            variant="primary"
            type="submit"
            className="btn-block align-self-center">
            Suivant
          </Button>
        </Row>

        <span className="signin-container">
          Vous possédez déjà un compte?{" "}
          <button className="signin-button" onClick={onSigninClick}>
            Connectez-vous
          </button>
        </span>
      </Form>
    </div>
  );
}

export default PreferenceForm;
