import React, { useEffect, useState } from "react";
import $ from "jquery";
import AfficherParcours from "../components/formationComponents/AfficherFormation";
import { useLocation, useNavigate } from "react-router-dom";
import { TailSpin } from "react-loader-spinner";
import { API_BASE_URL } from "../config/config.js";

const Formation = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [listeLosFormation, setListeLosFormation] = useState([]);
  const [listeLosNiveau, setListeLosNiveau] = useState([]);
  const [listeLosSansPrerequis, setListeLosSansPrerequis] = useState([]);
  const [resourcesList, setResourcesList] = useState([]);
  const [titleLo, setTitleLo] = useState([]);
  const location = useLocation();
  const navigate = useNavigate();

  const formData = location.state?.data;
  const [idTraining, setIdTraining] = useState(null);

  useEffect(() => {
    if (formData && formData.idTraining) {
      setIdTraining(formData.idTraining);
    } else {
      navigate("/training"); // Remplacez "/training" par le chemin de votre page de formation
    }
  }, [formData, navigate]);

  useEffect(() => {
    if (idTraining) {
      console.log("chargement des données de la formation");
      const fetchLosSansPrerequis = () => {
        return $.ajax({
          url: `${API_BASE_URL}/LosNoPrereq`,
          method: "POST",
          dataType: "json",
          data: { tr: idTraining },
        });
      };

      const fetchLosNiveau = () => {
        return $.ajax({
          url: `${API_BASE_URL}/listlo`,
          method: "GET",
          dataType: "json",
          headers: {
            "content-type": "application/json",
          },
          data: { tr: idTraining },
        });
      };

      const fetchRessourceList = () => {
        return $.ajax({
          url: `${API_BASE_URL}/listofAllPedagogicalResources`,
          method: "GET",
          dataType: "json",
          headers: {
            "content-type": "application/json",
          },
          data: { Train: idTraining },
        });
      };

      const fetchLosFormation = () => {
        return $.ajax({
          url: `${API_BASE_URL}/ListofCurLosWithPresq`,
          method: "POST",
          dataType: "json",
          data: { tr: idTraining },
        });
      };

      const fetchLosallLos = () => {
        return $.ajax({
          url: `${API_BASE_URL}/allLos`,
          method: "POST",
          dataType: "json",
          data: { tr: idTraining },
        });
      };

      Promise.all([
        fetchLosSansPrerequis(),
        fetchLosNiveau(),
        fetchLosFormation(),
        fetchLosallLos(),
        fetchRessourceList,
      ])
        .then(([response1, response2, response3, response4, response5]) => {
          setListeLosSansPrerequis(response1);
          setListeLosNiveau(response2);
          setListeLosFormation(response3);
          setTitleLo(response4);
          setResourcesList(JSON.parse(response5));
          const timer = setTimeout(() => {
            setIsLoading(false);
          }, 1000);
          return () => clearTimeout(timer);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [idTraining]);

  if (isLoading || !idTraining) {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}>
        <TailSpin color="#00BFFF" height={300} width={300} timeout={3000} />
        <p>Chargement... du parcours {formData?.idTraining}</p>
      </div>
    );
  }
  return (
    <AfficherParcours
      listeLosSansPrerequis={JSON.parse(listeLosSansPrerequis)}
      listeLosNiveau={JSON.parse(listeLosNiveau)}
      listeLosFormation={JSON.parse(listeLosFormation)}
      titleLo={JSON.parse(titleLo)}
      resourcesList={resourcesList}
      idTraining={idTraining}
      setResourcesList={setResourcesList}
      formData={formData}
    />
  );
};

export default Formation;
