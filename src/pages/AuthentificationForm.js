// 1. Module imports
import { useState } from "react";

// 2. Components imports
import SignupForm from "../components/userFormComponents/SignupForm";
import SigninForm from "../components/userFormComponents/SigninForm";

// 3. Style imports
import "./css/Sign.css";
import "react-toastify/dist/ReactToastify.css";
import ProtectedRoute from "../components/ProtectedRoute";

const FORMS = {
  NONE: "NONE",
  SIGNIN: "SIGNIN",
  SIGNUP: "SIGNUP",
};

const AuthentificationForm = (toast) => {
  const [activeForm, setActiveForm] = useState(FORMS.SIGNIN);
  const showSigninForm = () => setActiveForm(FORMS.SIGNIN);
  const showSignupForm = () => setActiveForm(FORMS.SIGNUP);
  const closeForms = () => setActiveForm(FORMS.NONE);

  return (
    <div className="App">
      <ProtectedRoute redirectLink="/" requireAuth={false} />
      <div className="sign-container">
        {activeForm === FORMS.SIGNIN && (
          <SigninForm onSignupClick={showSignupForm} />
        )}
        {activeForm === FORMS.SIGNUP && (
          <SignupForm
            toast={toast}
            onSigninClick={showSigninForm}
            onClose={closeForms}
          />
        )}
      </div>
    </div>
  );
};

export default AuthentificationForm;
