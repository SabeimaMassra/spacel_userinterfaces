import React, { useContext } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import Auth from "../contexts/Auth";

const HomePage = () => {
  const { isAuthenticated } = useContext(Auth);
  const navigate = useNavigate();

  const handleClick = () => {
    if (isAuthenticated) {
      navigate("/training");
    } else {
      navigate("/AuthentificationForm");
    }
  };

  return (
    <Container className="home-container">
      <Row>
        <Col className="home-content">
          <h1 className="home-title">Bienvenue sur la Plateforme SPACeL</h1>
          <p className="home-description">
            Le projet SPACeL vise à fournir aux apprenants un environnement
            d'apprentissage flexible et personnalisé, favorisant l'interaction
            et la collaboration entre les utilisateurs.
          </p>
          <p className="home-description">
            SPACeL offre des parcours d'apprentissage personnalisés, des
            recommandations basées sur l'intelligence artificielle, des
            fonctionnalités d'interaction et de collaboration entre les
            apprenants, un suivi de progression et une interface utilisateur
            conviviale.
          </p>
          <p className="home-description">
            Rejoignez-nous dès maintenant et découvrez une nouvelle façon
            d'apprendre en ligne !
          </p>
          <Button variant="primary" onClick={handleClick}>
            {isAuthenticated
              ? "Faire une nouvelle formation"
              : "S'authentifier"}
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

export default HomePage;
