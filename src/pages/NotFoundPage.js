import React from "react";
import { useNavigate } from "react-router-dom";
import { Container, Row, Col, Card, Button } from "react-bootstrap";

const NotFoundPage = () => {
  const navigate = useNavigate();

  const handleRedirect = () => {
    navigate("/"); // Redirection vers la page d'accueil
  };

  return (
    <Container fluid className="h-100">
      <Row className="h-100 align-items-center">
        <Col md={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body>
              <Card.Title>Page non trouvée</Card.Title>
              <Card.Text>Le lien que vous avez suivi n'existe pas.</Card.Text>
              <Button onClick={handleRedirect}>
                Retour à la page d'accueil
              </Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default NotFoundPage;
