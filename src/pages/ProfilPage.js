import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import ProtectedRoute from "../components/ProtectedRoute";
import "./css/ProfilPage.css"; // Importer le fichier CSS pour le style personnalisé

const LANGUAGES = [
  { code: "", label: "Choisir..." },
  { code: "en", label: "Anglais" },
  { code: "fr", label: "Français" },
  { code: "ar", label: "Arabe" },
  { code: "es", label: "Espagnol" },
  { code: "de", label: "Allemand" },
  { code: "it", label: "Italien" },
  { code: "ru", label: "Russe" },
  { code: "pt", label: "Portugais" },
  { code: "zh", label: "Chinois" },
  { code: "ja", label: "Japonais" },
  { code: "ko", label: "Coréen" },
  { code: "hi", label: "Hindi" },
];

const ProfilPage = () => {
  // Récupérer les données de l'utilisateur depuis la sessionStorage
  const userData = JSON.parse(sessionStorage.getItem("userData"));

  return (
    <div className="profil-page">
      <ProtectedRoute redirectLink="/AuthentificationForm" requireAuth={true} />
      <Container>
        <Row className="justify-content-center">
          <Col xs={12} md={10} lg={8}>
            <div className="profile-card">
              <h2 className="profile-heading">Profil de {userData.name}</h2>
              <p className="profile-info">ID utilisateur : {userData.idUser}</p>
              <p className="profile-info">
                Langues : {getUserLanguages(userData.languages)}
              </p>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

// Fonction pour obtenir les labels des langues sélectionnées par l'utilisateur
const getUserLanguages = (languages) => {
  const selectedLanguages = languages.map((languageCode) => {
    const language = LANGUAGES.find((lang) => lang.code === languageCode);
    return language ? language.label : "";
  });
  return selectedLanguages.join(", ");
};

export default ProfilPage;
