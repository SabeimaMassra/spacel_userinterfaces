import React, { useEffect, useState } from "react";
import { Container, Card, Modal } from "react-bootstrap";
import Graph from "react-graph-vis";
import $ from "jquery";
import { API_BASE_URL } from "../config/config";
import ProtectedRoute from "../components/ProtectedRoute";
import "./css/HistoriqueParcours.css";
import ModalLoContent from "../components/formationComponents/ModalLoContent";
import { json } from "react-router-dom";
import { TailSpin } from "react-loader-spinner";

const HistoriqueParcours = () => {
  const [data, setData] = useState([
    {
      subject:
        "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#UserCaroline",
      idP: "UserCarolineDEVPath",
      t: "DEV",
      lo: "LO1",
      p: "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#PersonalizedLearningPathUserCaroline",
      position: "1",
      dateMasterLO: "01/06/2023, 11:15:5011:15:50",
      idPedagogicalResource: "PR1",
    },
    {
      subject:
        "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#UserCaroline",
      idP: "UserCarolineDEVPath",
      t: "DEV",
      lo: "LO1",
      p: "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#PersonalizedLearningPathUserCaroline",
      position: "2",
      dateMasterLO: "01/06/2023, 11:16:4611:16:46",
      idPedagogicalResource: "PR2",
    },
    {
      subject:
        "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#UserCaroline",
      idP: "UserCarolineDEVPath",
      t: "DEV",
      lo: "LO2",
      p: "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#PersonalizedLearningPathUserCaroline",
      position: "2",
      dateMasterLO: "01/06/2023, 11:17:3811:17:38",
      idPedagogicalResource: "PR3",
    },
    {
      subject:
        "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#UserCaroline",
      idP: "UserCarolineDEVPath",
      t: "DEV",
      lo: "LO2",
      p: "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#PersonalizedLearningPathUserCaroline",
      position: "4",
      dateMasterLO: "01/06/2023, 11:18:1911:18:19",
      idPedagogicalResource: "PR4",
    },
    {
      subject:
        "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#UserCaroline",
      idP: "UserCarolineDEVPath",
      t: "DEV",
      lo: "LO2",
      p: "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#PersonalizedLearningPathUserCaroline",
      position: "3",
      dateMasterLO: "01/06/2023, 11:18:3311:18:33",
      idPedagogicalResource: "PR5",
    },
    {
      subject:
        "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#UserCaroline",
      idP: "UserCarolineDEVPath",
      t: "DEV",
      lo: "LO3",
      p: "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#PersonalizedLearningPathUserCaroline",
      position: "2",
      dateMasterLO: "01/06/2023, 11:19:1511:19:15",
      idPedagogicalResource: "PR6",
    },
    {
      subject:
        "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#UserCaroline",
      idP: "UserCarolineDEVPath2",
      t: "DEV2",
      lo: "LO1",
      p: "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#PersonalizedLearningPathUserCaroline",
      position: "1",
      dateMasterLO: "01/06/2023, 11:18:33",
      idPedagogicalResource: "PR7",
    },

    {
      subject:
        "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#UserCaroline",
      idP: "UserCarolineDEVPath2",
      t: "DEV2",
      lo: "LO2",
      p: "http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#PersonalizedLearningPathUserCaroline",
      position: "2",
      dateMasterLO: "01/06/2023, 11:19:33",
      idPedagogicalResource: "PR7",
    },
  ]);

  const [showModal, setShowModal] = useState(false);
  const [modalContent, setModalContent] = useState({});
  const [selectedResource, setSelectedResource] = useState(null);
  const [ressourceList, setRessourceList] = useState([
    {
      titlePedagogicalResource: "Ressource 1",
      idPedagogicalResource: "PR1",
      languagePedagogicalResource: "fr",
      formatPedagogicalResource: "V",
      levelPResource: 1,
      durationPResource: 13,
      urlPResource: "https://www.youtube.com/watch?v=XgVADKKb4jI",
    },
    {
      titlePedagogicalResource: "Ressource 2",
      idPedagogicalResource: "PR2",
      languagePedagogicalResource: "en",
      formatPedagogicalResource: "A",
      levelPResource: 2,
      durationPResource: 45,
      urlPResource: "https://example.com/resource2",
    },
    {
      titlePedagogicalResource: "Ressource 3",
      idPedagogicalResource: "PR3",
      languagePedagogicalResource: "fr",
      formatPedagogicalResource: "V",
      levelPResource: 3,
      durationPResource: 27,
      urlPResource: "https://www.youtube.com/watch?v=GU8kxJ3P67I",
    },
    {
      titlePedagogicalResource: "Ressource 4",
      idPedagogicalResource: "PR4",
      languagePedagogicalResource: "fr",
      formatPedagogicalResource: "V",
      levelPResource: 2,
      durationPResource: 30,
      urlPResource: "https://example.com/resource4",
    },
    {
      titlePedagogicalResource: "Ressource 5",
      idPedagogicalResource: "PR5",
      languagePedagogicalResource: "en",
      formatPedagogicalResource: "A",
      levelPResource: 3,
      durationPResource: 45,
      urlPResource: "https://www.youtube.com/watch?v=VIDEO_ID_5",
    },
    {
      titlePedagogicalResource: "Ressource 6",
      idPedagogicalResource: "PR6",
      languagePedagogicalResource: "es",
      formatPedagogicalResource: "T",
      levelPResource: 1,
      durationPResource: 60,
      urlPResource: "https://example.com/resource6",
    },
    {
      titlePedagogicalResource: "Ressource 7",
      idPedagogicalResource: "PR7",
      languagePedagogicalResource: "fr",
      formatPedagogicalResource: "V",
      levelPResource: 3,
      durationPResource: 30,
      urlPResource: "https://www.youtube.com/watch?v=VIDEO_ID_7",
    },
    {
      titlePedagogicalResource: "Ressource 8",
      idPedagogicalResource: "PR8",
      languagePedagogicalResource: "en",
      formatPedagogicalResource: "A",
      levelPResource: 1,
      durationPResource: 45,
      urlPResource: "https://example.com/resource8",
    },
    {
      titlePedagogicalResource: "Ressource 9",
      idPedagogicalResource: "PR9",
      languagePedagogicalResource: "es",
      formatPedagogicalResource: "T",
      levelPResource: 2,
      durationPResource: 60,
      urlPResource: "https://www.youtube.com/watch?v=VIDEO_ID_9",
    },
    {
      titlePedagogicalResource: "Ressource 10",
      idPedagogicalResource: "PR10",
      languagePedagogicalResource: "fr",
      formatPedagogicalResource: "V",
      levelPResource: 1,
      durationPResource: 30,
      urlPResource: "https://example.com/resource10",
    },
    {
      titlePedagogicalResource: "Ressource 11",
      idPedagogicalResource: "PR11",
      languagePedagogicalResource: "en",
      formatPedagogicalResource: "A",
      levelPResource: 2,
      durationPResource: 45,
      urlPResource: "https://www.youtube.com/watch?v=VIDEO_ID_11",
    },
  ]);

  const [graphs, setGraphs] = useState([]);

  var userData = JSON.parse(sessionStorage.getItem("userData"));
  var username = userData ? userData.name : "";
  var iduser = userData ? userData.idUser : 1;

  useEffect(() => {
    const fetchHistory = () => {
      return $.ajax({
        url: `${API_BASE_URL}/history`,
        method: "GET",
        dataType: "json",
        headers: {
          "content-type": "application/json",
        },
        data: { userLastname: username, idUser: iduser },
      });
    };

    const getRessources = (trainingTypes) => {
      const promises = trainingTypes.map((trainingType) => {
        return $.ajax({
          url: `${API_BASE_URL}/listofAllPedagogicalResources`,
          method: "GET",
          dataType: "json",
          headers: {
            "content-type": "application/json",
          },
          data: { Train: trainingType },
        });
      });

      return Promise.all(promises);
    };

    Promise.all([fetchHistory()])
      .then(([response1]) => {
        setData(json.parse(response1));
        const trainingTypes = [
          ...new Set(JSON.parse(response1).map((item) => item.t)),
        ];
        return getRessources(trainingTypes);
      })
      .then((responses) => {
        const resources = JSON.parse(responses).flat();
        setRessourceList(resources);
        const graphs = buildGraphs();
        setGraphs(graphs);
      })
      .catch((error) => {
        console.log(error);
        const graphs = buildGraphs();
        setGraphs(graphs);
      });
  }, [username]);

  const buildGraphs = () => {
    const uniquePaths = [...new Set(data.map((item) => item.idP))];
    const graphs = uniquePaths.map((path) => {
      const pathData = data.filter((item) => item.idP === path);
      const nodes = data
        .filter((item) => item.idP === path)
        .map((item, index) => {
          return {
            id: `${path}-node-${index}`,
            label: item.lo,
            title: item.lo,
            training: item.t,
            date: item.dateMasterLO,
            idPedagogicalResource: item.idPedagogicalResource,
            lo: item.lo, // Ajout des informations spécifiques du nœud
          };
        });

      const edges = nodes
        .map((node, index) => {
          if (index > 0) {
            return {
              id: `${path}-edge-${index}`,
              from: `${path}-node-${index - 1}`,
              to: `${path}-node-${index}`,
            };
          }
          return null;
        })
        .filter(Boolean);

      const graph = {
        nodes,
        edges,
        idP: path,
        dateMasterLO: pathData[0].dateMasterLO,
        training: pathData[0].t,
      };
      return graph;
    });

    return graphs;
  };

  const options = {
    layout: {
      hierarchical: true,
    },
    edges: {
      color: "#000000",
    },
    interaction: {
      zoomView: false,
      dragView: false,
      dragNodes: false,
    },
    height: "500px",
  };

  const events = {
    click: function (event) {
      const { nodes } = event;
      if (nodes.length === 1) {
        const nodeID = nodes[0];
        let clickedNode = null;
        for (const graph of graphs) {
          clickedNode = graph.nodes.find((node) => node.id === nodeID);
          if (clickedNode) {
            const { lo } = clickedNode;
            const resource = ressourceList.find(
              (item) =>
                item.idPedagogicalResource === clickedNode.idPedagogicalResource
            );
            setModalContent({
              ...clickedNode,
              resource,
            });

            setShowModal(true);
            break;
          }
        }
      }
    },
  };
  function formatDateTime(dateTime) {
    const parts = dateTime.split(",");
    const datePart = parts[0].trim();
    return datePart;
  }

  return (
    <Container className="historique-container">
      <h1 className="historique-title">Historique des Parcours</h1>
      <ProtectedRoute redirectLink="/AuthentificationForm" requireAuth={true} />
      {graphs.length > 0 ? (
        graphs.map((graph, index) => (
          <Card key={index} className="historique-card">
            <Card.Body>
              <Card.Title className="text-primary text-center">
                Parcours {graph.training} du :{" "}
                {formatDateTime(graph.dateMasterLO)}
              </Card.Title>

              <div className="graph-container">
                <Graph graph={graph} options={options} events={events} />
              </div>
            </Card.Body>
          </Card>
        ))
      ) : (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            height: "100vh",
          }}>
          <TailSpin color="#00BFFF" height={300} width={300} timeout={3000} />
          <p>Chargement de votre historique</p>
        </div>
      )}
      <Modal show={showModal} onHide={() => setShowModal(false)} size="lg">
        <ModalLoContent
          modalContent={modalContent}
          historique={true}
          setShowModal={setShowModal}
        />
      </Modal>
    </Container>
  );
};

export default HistoriqueParcours;
