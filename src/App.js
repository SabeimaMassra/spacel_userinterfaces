import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import AuthentificationForm from "./pages/AuthentificationForm";

import { useState } from "react";

import Auth from "./contexts/Auth";
import AuthNavBar from "./components/navbar/AuthNavBar";
import { toast, ToastContainer } from "react-toastify";
import ToastContext from "./contexts/ToastContext";

import Home from "./pages/Home";
import Admin from "./pages/AdminPage";
import NotFoundPage from "./pages/NotFoundPage";
import TrainingPage from "./pages/TrainingPage";
import Formation from "./pages/Formation";
import ProfilPage from "./pages/ProfilPage";
import HistoriqueParcours from "./pages/HistoriqueParcours";

function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(
    sessionStorage.getItem("isAuthenticated") === "true"
  );

  return (
    <Auth.Provider value={{ isAuthenticated, setIsAuthenticated }}>
      <ToastContext.Provider value={toast}>
        <Router>
          <div>
            <ToastContainer />
            <AuthNavBar />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route
                path="/AuthentificationForm"
                element={<AuthentificationForm />}
              />
              {/* <Route path="/admin" element={<Admin />} /> */}
              <Route path="/afficherFormation" element={<Formation />} />
              <Route path="/training" element={<TrainingPage />} />
              <Route path="/ProfilPage" element={<ProfilPage />} />
              <Route
                path="/historiqueParcours"
                element={<HistoriqueParcours />}
              />
              <Route path="*" element={<NotFoundPage />} />
            </Routes>
          </div>
        </Router>
      </ToastContext.Provider>
    </Auth.Provider>
  );
}
export default App;
